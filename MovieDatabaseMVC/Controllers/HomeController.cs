﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MovieDatabaseMVC.Models;

namespace MovieDatabaseMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            MovieDatabaseData movieDatabaseData = new MovieDatabaseData();
            if (movieDatabaseData.ListOfGenres == null)
            {
                movieDatabaseData.SelectGenresCheckBoxes(2);
            }
            
            ViewBag.Message = "";
            if (movieDatabaseData.Error != null)
                ViewBag.Message = movieDatabaseData.Error;
            ViewBag.Title = "Vyhledávání filmů";
            return View(movieDatabaseData);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Závěrečný úkol kurzu C#";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Kateřina Gojová";

            return View();
        }

        public ActionResult AddMovie()
        {
            MovieDatabaseData movieDatabaseData = new MovieDatabaseData();

            ViewBag.Title = "Přidávání filmů";
            ViewBag.Message = "";
            if (movieDatabaseData.Error != null)
            {
                ViewBag.Message = movieDatabaseData.Error;
            }
            else
            {
                movieDatabaseData.SelectGenresCheckBoxes(0);
                movieDatabaseData.CreateCountriesSelectList();
            }
            return View(movieDatabaseData);
        }

        [HttpPost]
        public ActionResult AddMovie(MovieDatabaseData movieDatabaseData, FormCollection formData)
        {
            if (ModelState.IsValid)
            {
                // 
                foreach (string key in formData.AllKeys) // pridam si zanry ze vstupnich poli do ListOfGenres
                {
                    if (key.Contains("newGenre"))
                    {
                        movieDatabaseData.AddNewGenreCheckBox(formData[key], 0, true, true);
                    }
                }

                if (formData.AllKeys.Contains("newCountry"))
                {
                    movieDatabaseData.Country = formData["newCountry"]; //pokud je zadana nova, select se ignoruje
                }
                ViewBag.Title = "Přidávání filmů";

                ViewBag.Message = movieDatabaseData.AddMovieJson();

                if (ViewBag.Message != "")
                {
                    movieDatabaseData.CreateCountriesSelectList();

                    if (ViewBag.Message == "Film byl přidán.")
                    {
                        movieDatabaseData.Name = null;
                        movieDatabaseData.Country = null;
                        movieDatabaseData.SelectGenresCheckBoxes(0);
                        ModelState.Clear();
                    }
                }
            }
            return View(movieDatabaseData);
        }

        [HttpPost]
        public ActionResult Index(MovieDatabaseData movieDatabaseData)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Title = "Vyhledávání filmů";
                // bylo neco zaskrtnuto?
                if (movieDatabaseData.ListOfGenres.Exists(r => r.IsChecked == true))
                {
                    // jsou uz naplneny zeme?
                    if (movieDatabaseData.ListOfCountries == null)
                    {
                        movieDatabaseData.SelectCountriesCheckBoxes(2);
                    }
                    else
                    {
                        if (movieDatabaseData.ListOfCountries.Exists(r => r.IsChecked == true))
                        {
                            //vypisu filmy
                            movieDatabaseData.PrintMovieTable(3);
                        }
                    }
                }

            }
            ModelState.Clear();
            return View(movieDatabaseData);
        }
    }

}