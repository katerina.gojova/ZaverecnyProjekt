﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web;



/*
Program po spuštění uživateli nabídne nejprve ze dvou náhodných žánrů, a poté ze dvou náhodných zemí. 
Na základě uživatelské volby program nabídne až tři náhodné volby.
Uživatel dostane volbu celý proces zopakovat.
Seznam filmů čtěte ze souboru Movies.json 
Bonus: Implementujte možnost přidat nový film. Tento se přidá do seznamu filmů v souboru.  
*/


namespace MovieDatabaseMVC.Models
{
    public class MovieDatabaseData
    {
        private MovieDatabase movieDatabase;
        [Display(Name = "Země")]
        public string Country { get; set; }
        [Display(Name = "Název")]
        public string Name { get; set; }
        public string Error { get; set; }
        public List<CheckBoxModel> ListOfGenres { get; set; }
        public List<CheckBoxModel> ListOfCountries { get; set; }
        public List<SelectListItem> CountriesSelectList { get; set; }
        public List<TableRowModel> TableOfMovies { get; set; } //prevod do tabulky
        private string jsonFile = HttpContext.Current.Server.MapPath("~/Models/Movies.json");

        public MovieDatabaseData()
        {
            movieDatabase = LoadJson();
        }

        private MovieDatabase LoadJson()
        {
            try
            {
                using (StreamReader r = new StreamReader(jsonFile))
                {
                    string json = r.ReadToEnd();
                    return JsonConvert.DeserializeObject<MovieDatabase>(json);
                }
            }
            catch (Exception)
            {
                Error = "Nepodarilo se nacist soubor";
                return null;

            } 
        }


        public string AddMovieJson()
        {
            if (Name == null)
            {
                return "Pole pro název filmu musíte vyplnit!";
            }

            if (Country == null || Country == "")
            {
                return "Pole pro zemi musíte vyplnit!";
            }

            Name = Name.Trim();
            string pattern = @"^([a-žA-Ž0-9#@]+[a-žA-Ž0-9-.,!?#@\s]+)$"; ///na ledacos (#,@) krome mezery, pak jde pouzit i mezera, pomlcka, tecka,carka, vykricnik, otaznik - nazev filmu
            Regex rgx = new Regex(pattern);
            bool match = rgx.IsMatch(Name);
            if (!match)
            {
                return "Pole pro název filmu není validní (obsahuje nepovolenou kombinaci znaků)!";
            }

            Country = Country.Trim();
            pattern = @"^([a-žA-Ž]+[a-žA-Ž-\s]+)$"; ///na zacatku pismeno, pak jde pouzit i mezera nebo pomlcka - zeme
            rgx = new Regex(pattern);
            match = rgx.IsMatch(Country);
            if (!match)
            {
                return "Pole pro název země není validní (obsahuje nepovolenou kombinaci znaků)!";
            }

            if (!movieDatabase.Movies.Exists(r => (r.name == Name && r.country == Country)))
            {
                List<string> selectedGenres = new List<string>();
                var lists = SelectChecked(ListOfGenres); //aktualizace seznamu pouze na vybrané zanry
                selectedGenres = lists.Item1; // seznam s pridavanymi zanry
                ListOfGenres = lists.Item2;

                if (selectedGenres.Count == 0)
                {
                    return "Musíte zadat nějaký žánr";
                }
                //vsechno mam pripraveno, muzu zapsat film
                Movie movie = new Movie();
                movie.name = Name;
                movie.country = Country;
                movie.genre = selectedGenres;
                movieDatabase.Movies.Add(movie);
                try
                {
                    string jsonData = JsonConvert.SerializeObject(movieDatabase);
                    using (StreamWriter wr = new StreamWriter(jsonFile))
                    {
                        wr.Write(jsonData);
                    }
                }
                catch (Exception)
                {
                    return "Nepodařilo se zapsat data do souboru.";
                    throw;
                }
                return "Film byl přidán.";
            }
            else
            {
                return "Tento film už v databázi existuje";
            }
        }

        private Tuple<List<string>, List<CheckBoxModel>> SelectChecked(List<CheckBoxModel> list)
        {
            List<string> selectedItems = new List<string>();
            List<CheckBoxModel> list2 = new List<CheckBoxModel>(); // na remove mi to padá, tak proto takhle
            foreach (CheckBoxModel item in list)
            {
                if (item.IsChecked)
                {
                    list2.Add(item);
                    selectedItems.Add(item.Text);
                }
            }
            return new Tuple<List<string>, List<CheckBoxModel>>(selectedItems, list2);
        }

        public bool AddNewGenreCheckBox(string text, int value, bool isChecked, bool fromTextInput)
        {
            if (fromTextInput)
            {
                //validace
                text = text.Trim();
                string pattern = @"^([a-žA-Ž0-9]+[a-žA-Ž0-9-\s]+)$"; //na zacatku pismeno nebo cislice, pak jde pouzit i mezera nebo pomlcka - zanr
                Regex rgx = new Regex(pattern);
                bool match = rgx.IsMatch(text);
                if (!match)
                {
                    return false;
                }
                //zjisteni, jestli uz existuje
                if (ListOfGenres.Exists(r => r.Text == text))
                {
                    return false;
                }
            }
            ListOfGenres.Add(new CheckBoxModel { Text = text, Value = value, IsChecked = isChecked });
            return true;
        }

        public bool AddNewCountryCheckBox(string text, int value, bool isChecked)
        {
            ListOfCountries.Add(new CheckBoxModel { Text = text, Value = value, IsChecked = isChecked });
            return true;
        }

        public void SelectGenresCheckBoxes(int count)// 0 a mensi cislo = vsechny 
        {
            if (movieDatabase != null)
            {
                ListOfGenres = new List<CheckBoxModel>();
                List<string> selectedGenres = new List<string>();

                if (count > 0) // pro vyhledavani
                {
                    selectedGenres = movieDatabase.SelectRandGenres(count);
                }
                else // pro zapis filmu
                {
                    selectedGenres = movieDatabase.SelectAllGenres();
                    // musim pridat dalsi textova pole pro nove zanry 
                }
                // naplnim ListOfGenres nahodnymi zanry - generuji se primo checkboxy
                for (int i = 0; i < selectedGenres.Count; i++)
                {
                    AddNewGenreCheckBox(selectedGenres[i], i, false, false);
                }
            }
        }

        public void SelectCountriesCheckBoxes(int count)
        {
            List<string> selectedGenres = new List<string>();
            var lists = SelectChecked(ListOfGenres); //aktualizace seznamu pouze na vybrané zanry
            selectedGenres = lists.Item1;
            ListOfGenres = lists.Item2;
            //count 0 a mensi cislo = vsechny
            ListOfCountries = new List<CheckBoxModel>();
            List<string> selectedCountries = new List<string>();
            if (count > 0)
            {
                selectedCountries = movieDatabase.SelectRandCountries(count, selectedGenres);
            }
            else
            {
                selectedCountries = movieDatabase.SelectAllCountries();
            }

            // naplnim ListOfCountries nahodnymi zememi - generuji checkboxy
            for (int i = 0; i < selectedCountries.Count; i++)
            {
                AddNewCountryCheckBox(selectedCountries[i], i, false);
            }
        }

        public void PrintMovieTable(int count)
        {
            List<string> selectedCountries = new List<string>();

            List<string> selectedGenres = new List<string>();
            foreach (CheckBoxModel item in ListOfGenres)
            {
                selectedGenres.Add(item.Text);
            }

            var lists = SelectChecked(ListOfCountries); //aktualizace seznamu pouze na vybrané zeme
            selectedCountries = lists.Item1;
            ListOfCountries = lists.Item2;

            List<Movie> listOfMovies = movieDatabase.SelectRandMovies(selectedGenres, selectedCountries, 3);
            // vytvoreni tabulky
            TableOfMovies = new List<TableRowModel>();
            foreach (Movie movie in listOfMovies)
            {
                string genreString = "";
                foreach (string g in movie.genre)
                {
                    genreString = genreString + g + ", ";
                }
                genreString = genreString.Substring(0, genreString.Length - 2);
                TableOfMovies.Add(new TableRowModel { Name = movie.name, Country = movie.country, Genre = genreString });
            }
        }

        public void CreateCountriesSelectList()
        {
            CountriesSelectList = new List<SelectListItem>();
            CountriesSelectList.Add(new SelectListItem { Text = "--vyberte zemi--", Value = "", Selected = true });
            foreach (string country in movieDatabase.SelectAllCountries())
            {
                CountriesSelectList.Add(new SelectListItem { Text = country, Value = country, Selected = false });
            }
        }
    }
}