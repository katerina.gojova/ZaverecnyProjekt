﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieDatabaseMVC.Models
{
    public class Movie
    {
        public string name { get; set; }
        public string country { get; set; }
        public List<string> genre { get; set; }
    }
}
