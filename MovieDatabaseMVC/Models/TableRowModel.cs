﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieDatabaseMVC.Models
{
    public class TableRowModel
    {
        public string Name
        {
            get;
            set;
        }
  
        public string Country
        {
            get;
            set;
        }

        public string Genre
        {
            get;
            set;
        }
    }
}