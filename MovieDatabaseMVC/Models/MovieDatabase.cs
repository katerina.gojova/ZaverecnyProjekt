﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;


namespace MovieDatabaseMVC.Models
{
    public class MovieDatabase
    {
        public List<Movie> Movies { get; set; }

        public List<string> SelectAllGenres()
        {
            List<string> genres = new List<string>();
            foreach (Movie movie in Movies)
            {
                foreach (string genre in movie.genre)
                {
                    genres.Add(genre);
                }
            }
            genres = genres.Distinct().ToList();
            return genres;
        }

        public List<string> SelectAllCountries()
        {
            List<string> countries = new List<string>();
            foreach (Movie movie in Movies)
            {
                countries.Add(movie.country);
            }
            countries = countries.Distinct().ToList();
            return countries;
        }

        public List<string> SelectRandGenres(int count) //vybere libovolny pocet nahodnych zanru
        {
            Random rnd = new Random();
            List<string> genres = SelectAllGenres();
            List<string> genres2 = new List<string>();

            //tady musim osetrit, jestli je jich tam dost
            if (genres.Count > count)
            {
                while (genres2.Count < count)
                {
                    int index = rnd.Next(genres.Count);
                    string genre = genres[index];
                    if (!genres2.Contains(genre))
                    {
                        genres2.Add(genre);
                    }
                }
            }
            else
            {
                genres2 = genres;
            }

            return genres2;
        }

        public List<string> SelectRandCountries(int count, List<string> selectedGenres) //vybere libovolny pocet nahodnych zemi podle vybranych zanru
        {
            List<string> countries = new List<string>();
            foreach (Movie movie in Movies)
            {
                foreach (string genre in movie.genre)
                {
                    if (selectedGenres.Contains(genre) && !countries.Contains(movie.country)) // pokud uz tuto zemi v seznamu nemam a video je daneho zanru, pridam zemi z tohoto videa
                    {
                        countries.Add(movie.country);
                    }
                }
            }

            Random rnd = new Random(); // necham si pouze par (count) nahodnych 
            List<string> countries2 = new List<string>();

            // osetrit, jestli jich tam je dost
            if (countries.Count > count)
            {
                while (countries2.Count < count)
                {
                    int index = rnd.Next(countries.Count);
                    string genre = countries[index];
                    if (!countries2.Contains(genre)) // pro pripad, ze se 2x vygeneruje stejny index
                    {
                        countries2.Add(genre);
                    }
                }
            }
            else
            {
                countries2 = countries;
            }
            return countries2;
        }

        public List<Movie> SelectRandMovies(List<string> selectedGenres, List<string> selectedCountries, int count)
        {
            List<Movie> movies2 = new List<Movie>();
            foreach (Movie movie in Movies)
            {
                if (selectedCountries.Contains(movie.country)) //ptám se na zemi
                {
                    foreach (string genre in movie.genre)
                    {
                        if (selectedGenres.Contains(genre) && !movies2.Contains(movie)) 
                        {
                                movies2.Add(movie);
                        }
                    }
                }
            }
            // jeste vybrat maximalně několik (count) nahodných filmů
            List<Movie> movies3 = new List<Movie>();
            if (movies2.Count > count)
            {
                // vybiram
                Random rnd = new Random();
                while (movies3.Count < count)
                {
                    int index = rnd.Next(movies2.Count);
                    Movie movie = movies2[index];
                    if (!movies3.Contains(movie)) // pro pripad, ze se 2x vygeneruje stejny index
                    {
                        movies3.Add(movie);
                    }
                }
            }
            else
            {
                // vypisu vse
                movies3 = movies2;
            }
            return movies3;
        }
    }
}
